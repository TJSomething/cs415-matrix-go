package main

import (
	"../matrix"
	"fmt"
	"os"
	"time"
)

func main() {
	if len(os.Args) == 4 {
		// Get the matrices
		mat1, err1 := matrix.CSVFileToMatrix(os.Args[1])
		mat2, err2 := matrix.CSVFileToMatrix(os.Args[2])
		if err1 != nil {
			panic(err1)
		}
		if err2 != nil {
			panic(err2)
		}
		// Start the timer
		startTime := time.Now()
		// Multiply the matrices
		mat3 := mat1.Multiply(mat2)
		// Print the time taken
		fmt.Print(time.Since(startTime).Seconds())
		// Write the result
		mat3.ToCSVFile(os.Args[3])
	} else {
		fmt.Print("Usage: ")
		fmt.Printf("%s <matrix 1> <matrix 2> <result>\n", os.Args[0])
		fmt.Print("Matrices are expected to be in unquoted CSV format.\n")
	}
}
