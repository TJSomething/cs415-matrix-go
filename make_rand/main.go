package main 

import (
	"os"
	"../matrix"
	"strconv"
	"math/rand"
	"fmt"
)

func main() {
	if (len(os.Args) == 4) {
		// Make a matrix
		rows, _ := strconv.Atoi(os.Args[1])
		cols, _ := strconv.Atoi(os.Args[2])
		mat := matrix.BuildMatrix(rows,cols, func (i,j int) (float64) {
			return rand.Float64() 
		})
		// Write the matrix
		mat.ToCSVFile(os.Args[3])
	} else {
		fmt.Print("Usage: ")
		fmt.Printf("%s <rows> <cols> <result>\n", os.Args[0])
		fmt.Print("Matrices are expected to be in unquoted CSV format.\n")
	}
}

