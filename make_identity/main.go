package main 

import (
	"os"
	"../matrix"
	"strconv"
	"fmt"
)

func main() {
	if (len(os.Args) == 3) {
		// Make a matrix
		size, _ := strconv.Atoi(os.Args[1])
		mat := matrix.BuildMatrix(size, size, func (i,j int) (float64) {
			if i!=j {
				return 0
			}
			return 1
		})
		// Write the matrix
		mat.ToCSVFile(os.Args[2])
	} else {
		fmt.Print("Usage: ")
		fmt.Printf("%s <size> <result>\n", os.Args[0])
		fmt.Print("Matrices are expected to be in unquoted CSV format.\n")
	}
}

