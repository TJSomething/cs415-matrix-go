package mpi

/*
#cgo pkg-config: ompi-c
#include <mpi.h>
#include <stdlib.h>

// Due to weird linker stuff, we need to grab this all at runtime
MPI_Comm get_mpi_comm_world() {
	return MPI_COMM_WORLD;
}

MPI_Datatype get_mpi_int() {
	return MPI_INT;
}
MPI_Datatype get_mpi_byte() {
	return MPI_BYTE;
}
MPI_Datatype get_mpi_float() {
	return MPI_FLOAT;
}
MPI_Datatype get_mpi_double() {
	return MPI_DOUBLE;
}
*/
import "C"

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"reflect"
	"sync/atomic"
	"unsafe"
)

const (
	AnyTag    = C.MPI_ANY_TAG
	AnySource = C.MPI_ANY_SOURCE
)

const (
	byteFlag   = iota
	intFlag    = iota
	floatFlag  = iota
	doubleFlag = iota
	gobFlag    = iota
)

// Grab the various things from C at the start
var commWorld = C.get_mpi_comm_world()
var mpiByte = C.get_mpi_byte()
var mpiInt = C.get_mpi_int()
var mpiFloat = C.get_mpi_float()
var mpiDouble = C.get_mpi_double()
var maxTag int

func Init(args []string) (newArgs []string, err int) {
	// We need some l-values that MPI_Init can modify if it needs to
	argc := C.int(len(os.Args))
	argv := make([]*C.char, argc)
	for i, arg := range os.Args {
		argv[i] = C.CString(arg)
	}
	argvPtr := &argv[0]
	// Run MPI_Init_thread
	var provided C.int
	err = int(C.MPI_Init_thread(&argc, &argvPtr, C.MPI_THREAD_MULTIPLE, &provided))
	// Get the arguments back out
	newArgs = make([]string, argc)
	for i, arg := range argv {
		newArgs[i] = C.GoString(arg)
		C.free(unsafe.Pointer(arg))
	}
	// Get values that can only be gotten after MPI_Init
	maxTag, _ = commGetAttr(C.MPI_TAG_UB)
	return newArgs, err
}

func Finalize() (err int) {
	err = int(C.MPI_Finalize())
	return err
}

func CommRank() (rank int, err int) {
	var cRank C.int
	err = int(C.MPI_Comm_rank(commWorld, &cRank))
	return int(cRank), err
}

func CommSize() (size int, err int) {
	var cSize C.int
	err = int(C.MPI_Comm_size(commWorld, &cSize))
	return int(cSize), err
}

func commGetAttr(key int) (val, err int) {
	var cVal, flag C.int
	err = int(C.MPI_Comm_get_attr(commWorld, C.int(key), unsafe.Pointer(&cVal), &flag))
	return int(cVal), err
}

/* This is used to allow us to provide thread safety on sends and receives. 
   In order to allow variable length messages, all transactions are done in
   two steps, sending the length and sending the data. In order to ensure that
   two receives with the same node and tag, running in different threads, do
   not have problems with the length for one being confused with the messageif maxUsedTag < tag {
		maxUsedTag = tag
	}
   for the other, messages are sent with different tags that are sent along
   with their lengths. These tags are randomly chosen from the set
   (maxUsedTag, MPI_TAG_UB]. This is slow because it spins and should probably
   be replaced with channels.*/
var maxUsedTag int32 = 0

func updateMaxUsedTag(tag int32) {
	for swapped := false; !swapped; {
		oldMax := maxUsedTag
		if tag > oldMax {
			swapped = atomic.CompareAndSwapInt32(&maxUsedTag, oldMax, tag)
		} else {
			// This works because, if maxUsedTag has changed since before, it
			// has to be larger, which means we still don't need to change it.
			swapped = true
		}
	}
}

type MPIError struct {
	errorCode C.int
}

func (err MPIError) Error() string {
	var strLen C.int
	var errCString [C.MPI_MAX_ERROR_STRING]C.char
	C.MPI_Error_string(err.errorCode, &errCString[0], &strLen)
	return fmt.Sprintf("MPI Error: %s", C.GoStringN(&errCString[0], strLen))
}

var Register = gob.Register

func Send(message interface{}, dest int, tag int32) (err error) {
	var buf bytes.Buffer
	var messageType int
	var mpiMessageType C.MPI_Datatype
	var frozenBuf []byte
	var arrayPointer unsafe.Pointer
	var length C.int

	// Make sure it's not possible to use the current tag for a
	// message tag.
	updateMaxUsedTag(tag)

	// Find the type of the data to see if we can send it raw
	//switch ptr := message.(type) {
	// This is required to do typecasts nicely
	//case interface{}:
	switch m := message.(type) {
	case *[]int: // I'm assuming that int == C.int
		//fmt.Printf("Type being sent: %v\n", reflect.ValueOf(message).Type())
		messageType = intFlag
		mpiMessageType = mpiInt
		if len(*m) > 0 {
			arrayPointer = unsafe.Pointer(&(*m)[0])
		} else {
			arrayPointer = unsafe.Pointer(nil)
		}
		length = C.int(len(*m))
	case *[]float64:
		//fmt.Printf("Type being sent: %v\n", reflect.ValueOf(message).Type())
		messageType = doubleFlag
		mpiMessageType = mpiDouble
		if len(*m) > 0 {
			arrayPointer = unsafe.Pointer(&(*m)[0])
		} else {
			arrayPointer = unsafe.Pointer(nil)
		}
		length = C.int(len(*m))
	case *[]float32:
		//fmt.Printf("Type being sent: %v\n", reflect.ValueOf(message).Type())
		messageType = floatFlag
		mpiMessageType = mpiFloat
		if len(*m) > 0 {
			arrayPointer = unsafe.Pointer(&(*m)[0])
		} else {
			arrayPointer = unsafe.Pointer(nil)
		}
		length = C.int(len(*m))
	case *[]byte:
		//fmt.Printf("Type being sent: %v\n", reflect.ValueOf(message).Type())
		messageType = byteFlag
		mpiMessageType = mpiByte
		if len(*m) > 0 {
			arrayPointer = unsafe.Pointer(&(*m)[0])
		} else {
			arrayPointer = unsafe.Pointer(nil)
		}
		length = C.int(len(*m))
	default: // If not, we can always serialize it
		//fmt.Printf("Type being sent: %v\n", reflect.ValueOf(message).Type())
		// Serialize the data into a buffer
		enc := gob.NewEncoder(&buf)
		err = enc.Encode(message)
		if err != nil {
			return err
		}
		frozenBuf = buf.Bytes()

		messageType = gobFlag
		mpiMessageType = mpiByte
		arrayPointer = unsafe.Pointer(&frozenBuf[0])
		length = C.int(len(frozenBuf))
	}

	// Generate a message tag
	messageTag := rand.Int31n(int32(maxTag)-maxUsedTag-1) + maxUsedTag + 1
	// Send the size, tag, and type of the message
	var headBuf [3]C.int
	headBuf[0] = length
	headBuf[1] = C.int(messageTag)
	headBuf[2] = C.int(messageType)

	//fmt.Printf("Sending message header (size=%d, tag=%d)...\n", headBuf[0], headBuf[1])
	cErr := C.MPI_Ssend(unsafe.Pointer(&(headBuf[0])),
		3,
		mpiInt,
		C.int(dest),
		C.int(tag),
		commWorld)
	if cErr != 0 {
		return MPIError{cErr}
	}

	//fmt.Printf("Sending %d byte message wtih tag %d to %d...\n", length, tag, dest)
	// Send the message
	cErr = C.MPI_Ssend(arrayPointer,
		length,
		mpiMessageType,
		C.int(dest),
		C.int(messageTag),
		commWorld)
	if cErr != 0 {
		return MPIError{cErr}
	}
	//fmt.Printf("Sent %d byte message\n", length)
	return nil
}

// message must be a pointer to the location that we are putting the data
func Recv(message interface{}, src int, tag int32) (stat Status, err error) {
	// Make sure that the tag selection is safe
	updateMaxUsedTag(tag)

	// Receive the header 
	//fmt.Printf("Receiving message header from %d...\n", src)
	var headBuf [3]C.int // {size, tag, type}
	var status C.MPI_Status
	cErr := C.MPI_Recv(unsafe.Pointer(&headBuf[0]),
		3,
		mpiInt,
		C.int(src),
		C.int(tag),
		commWorld,
		&status)
	if cErr != 0 {
		return makeStatus(status), MPIError{cErr}
	}
	// If we don't have it, grab the actual source
	if src == AnySource {
		src = int(status.MPI_SOURCE)
	}

	// Find out the type of the next message
	typeFlag := int(headBuf[2])
	switch typeFlag {
	default:
		return makeStatus(status), errors.New("Invalid type sent")
	case gobFlag:
		// Receive the message
		//fmt.Printf("Receiving %d byte message with tag %d from %d...\n", headBuf[0], headBuf[1], src)
		messageBytes := make([]byte, headBuf[0])
		cErr = C.MPI_Recv(unsafe.Pointer(&messageBytes[0]),
			headBuf[0],
			mpiByte,
			C.int(src),
			headBuf[1],
			commWorld,
			&status)
		if cErr != 0 {
			return makeStatus(status), MPIError{cErr}
		}
		// Unpack the message
		buf := bytes.NewBuffer(messageBytes)
		dec := gob.NewDecoder(buf)
		err = dec.Decode(message)
		if err != nil {
			return makeStatus(status), err
		}
	case intFlag:
		//fmt.Printf("Receiving %d ints...\n", headBuf[0])
		var slicePtr unsafe.Pointer
		messageSlice := make([]int, headBuf[0])
		// This make sure that we don't try to index a zero-length array
		if headBuf[0] > 0 { //
			slicePtr = unsafe.Pointer(&messageSlice[0])
		}
		cErr = C.MPI_Recv(slicePtr,
			headBuf[0],
			mpiInt,
			C.int(src),
			headBuf[1],
			commWorld,
			&status)
		if cErr != 0 {
			return makeStatus(status), MPIError{cErr}
		}
		// We need to do some reflection voodoo to set message
		// to our slice without the type
		value := reflect.ValueOf(message)
		value.Elem().Set(reflect.ValueOf(messageSlice))
	case floatFlag:
		//fmt.Printf("Receiving %d floats...\n", headBuf[0])
		var slicePtr unsafe.Pointer
		messageSlice := make([]float32, headBuf[0])
		// This makes sure that we don't try to index a zero-length array
		if headBuf[0] > 0 { //
			slicePtr = unsafe.Pointer(&messageSlice[0])
		}
		cErr = C.MPI_Recv(slicePtr,
			headBuf[0],
			mpiFloat,
			C.int(src),
			headBuf[1],
			commWorld,
			&status)
		if cErr != 0 {
			return makeStatus(status), MPIError{cErr}
		}
		value := reflect.ValueOf(message)
		value.Elem().Set(reflect.ValueOf(messageSlice))
	case doubleFlag:
		var slicePtr unsafe.Pointer
		messageSlice := make([]float64, headBuf[0])
		// This makes sure that we don't try to index a zero-length array
		if headBuf[0] > 0 { //
			slicePtr = unsafe.Pointer(&messageSlice[0])
		}
		cErr = C.MPI_Recv(slicePtr,
			headBuf[0],
			mpiDouble,
			C.int(src),
			headBuf[1],
			commWorld,
			&status)
		if cErr != 0 {
			return makeStatus(status), MPIError{cErr}
		}
		value := reflect.ValueOf(message)
		value.Elem().Set(reflect.ValueOf(messageSlice))
	case byteFlag:
		var slicePtr unsafe.Pointer
		messageSlice := make([]byte, headBuf[0])
		// This makes sure that we don't try to index a zero-length array
		if headBuf[0] > 0 { //
			slicePtr = unsafe.Pointer(&messageSlice[0])
		}
		cErr = C.MPI_Recv(slicePtr,
			headBuf[0],
			mpiByte,
			C.int(src),
			headBuf[1],
			commWorld,
			&status)
		if cErr != 0 {
			return makeStatus(status), MPIError{cErr}
		}
		value := reflect.ValueOf(message)
		value.Elem().Set(reflect.ValueOf(messageSlice))
	}
	//fmt.Printf("Received %v from %d\n", message, src)
	return makeStatus(status), nil
}

func makeStatus(cStat C.MPI_Status) (stat Status) {
	stat.source = int(cStat.MPI_SOURCE)
	stat.tag = int(cStat.MPI_TAG)
	stat.error = int(cStat.MPI_ERROR)
	return stat
}

type Status struct {
	source, tag, error int
}
