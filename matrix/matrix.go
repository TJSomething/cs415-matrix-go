package matrix

import (
	"bytes"
	"encoding/csv"
	"encoding/gob"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
)

type Matrix struct {
	Rows, Cols int
	Elements   []float64
}

func NewMatrix(rows int, cols int) (m *Matrix) {
	m = new(Matrix)
	m.Rows = rows
	m.Cols = cols
	m.Elements = make([]float64, rows*cols)
	return m
}

func (m *Matrix) Set(r int, c int, e float64) {
	if r < m.Rows && c < m.Cols {
		m.Elements[r*m.Cols+c] = e
	}
	return
}

func (m *Matrix) Get(r int, c int) float64 {
	if r < m.Rows && c < m.Cols {
		return m.Elements[r*m.Cols+c]
	}
	return math.NaN()
}

func (x *Matrix) Multiply(y *Matrix) (result *Matrix) {
	if x.Cols == y.Rows {
		result := NewMatrix(x.Rows, y.Cols)
		for i := int(0); i < x.Rows; i++ {
			for j := int(0); j < y.Cols; j++ {
				acc := float64(0)
				for k := int(0); k < x.Cols; k++ {
					acc += x.Get(i, k) * y.Get(k, j)
				}
				result.Set(i, j, acc)
			}
		}
		return result
	}
	return nil
}

type MatrixError struct {
	message string
}

func (e MatrixError) Error() string {
	return e.message
}

func CSVFileToMatrix(filename string) (m *Matrix, err error) {
	// Allocate
	entry := float64(0)
	// Open file
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()
	reader := csv.NewReader(file)
	// We need to grab the first row early to be able to check the number of 
	// columns
	row, err := reader.Read()
	// Initialize values to accumulate
	cols := len(row)
	mat := NewMatrix(0, cols)
	// For every row
	for ; err != io.EOF; row, err = reader.Read() {
		// If the row is the right length
		switch len(row) {
		case cols:
			// For every cell in the row
			for _, entryString := range row {
				// Convert the cell into a float and put it in the matrix
				entry, err = strconv.ParseFloat(entryString, 64)
				if err == nil {
					mat.Elements = append(mat.Elements, entry)
				} else {
					err = MatrixError{"Could not parse cell"}
					return
				}
			}
			// We're done with a row, so note that we have a new row
			mat.Rows += 1
		// If the row is empty, assume that we're done
		case 0:
			return mat, nil
		default:
			return nil, MatrixError{
				fmt.Sprintf("Mismatched row length on row %d: %d != %d",
					mat.Rows+1, len(row), cols)}
		}
	}
	return mat, nil
}

func (m *Matrix) ToCSVFile(filename string) {
	file, _ := os.Create(filename)
	defer file.Close()
	writer := csv.NewWriter(file)
	rowBuffer := make([]string, m.Cols)
	for i := 0; i < m.Rows; i++ {
		for j := 0; j < m.Cols; j++ {
			rowBuffer[j] = fmt.Sprint(m.Get(i, j))
		}
		writer.Write(rowBuffer)
	}
	// File gets truncated without this
	writer.Flush()
}

func BuildMatrix(rows, cols int, setter func(int, int) float64) (m *Matrix) {
	m = NewMatrix(rows, cols)
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			//fmt.Printf("%d,%d\n",i,j)
			m.Set(i, j, setter(i, j))
		}
	}
	return m
}

func (m *Matrix) GobDecode(data []byte) (err error) {
	buf := bytes.NewBuffer(data)
	dec := gob.NewDecoder(buf)
	err = dec.Decode(&m.Rows)
	if err != nil {
		return err
	}
	err = dec.Decode(&m.Cols)
	if err != nil {
		return err
	}
	err = dec.Decode(&m.Elements)
	if err != nil {
		return err
	}
	return nil
}

func (m Matrix) GobEncode() (out []byte, err error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(&m.Rows)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(&m.Cols)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(&m.Elements)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func (m Matrix) GetSubMatrix(top, left, bottom, right int) Matrix {
	result := NewMatrix(bottom-top+1, right-left+1)
	for i := 0; i < bottom-top+1; i++ {
		for j := 0; j < right-left+1; j++ {
			result.Set(i, j, m.Get(top+i, left+j))
		}
	}
	return *result
}
