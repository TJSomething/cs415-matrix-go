package main

import (
	"../matrix"
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	if len(os.Args) == 4 {
		// Make a matrix
		rows, _ := strconv.Atoi(os.Args[1])
		inner, _ := strconv.Atoi(os.Args[2])
		cols, _ := strconv.Atoi(os.Args[3])
		mat1 := matrix.NewMatrix(rows, inner)
		mat2 := matrix.NewMatrix(inner, cols)
		// Start the timer
		startTime := time.Now()
		// Multiply the matrices
		mat1.Multiply(mat2)
		// Print the time taken
		endTime := time.Now()
		fmt.Printf("%v", endTime.Sub(startTime).Seconds())
	} else {
		fmt.Print("Usage: ")
		fmt.Printf("%s <A> <B> <C>\n", os.Args[0])
		fmt.Print("This tests the speed of multiplying a random AxB matrix and a BxC matrix.\n")
	}
}
