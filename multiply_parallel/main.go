package main

import (
	"../matrix"
	"../mpi"
	"container/heap"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"
)

// State machine states!
const ( // For masters
	MStart = iota
	MReceivedReady
	MToGetWork
	MGotWork
	MSentHeader
	MToCheckKillSignal
	MGotKillSignal
	MReceivedDyingWorkRequest
	MDead
)

const ( // For slaves
	SStart = iota
	SRequestingMatrix
	SRequestedMatrix
	SGotResult
	SNoFinished
	SRequestedWork
	SReceivedWork
	SReceivedKill
	SWorkerNotYetDead
	SKilledWorker
	SReceivedNothing
	SDead
)

const (
	realMaxQueue = 1024
)

func main() {
	args, _ := mpi.Init(os.Args)
	// Get the rank of the process
	rank, _ := mpi.CommRank()
	count, _ := mpi.CommSize()
	// Profiling
	/*profFile, err := os.Create(fmt.Sprintf("profile%0d.prof", rank))
	if err == nil {
		defer profFile.Close()
		pprof.StartCPUProfile(profFile)
		defer pprof.StopCPUProfile()
	}*/
	// Register the message types for serialization
	mpi.Register(matrix.Matrix{})
	mpi.Register(WorkUnit{})
	mpi.Register(make([]WorkUnit, 1))
	mpi.Register(Work{})
	mpi.Register(WorkRequest{})
	mpi.Register(Result{})
	mpi.Register(NoWork{})
	mpi.Register(Kill{})
	mpi.Register(KillACK{})
	mpi.Register([]matrix.Matrix{})
	mpi.Register(MatrixRequest{})
	// Run master
	switch rank {
	case 0:
		master(args)
	// The last slave should be on a different box, which is ideal for
	// a ping test
	case count - 1:
		slave1()
	default:
		slave()
	}
	mpi.Finalize()
}

func master(args []string) {
	var message interface{}

	// Parse the arguments
	if len(args) != 4 {
		panic("Invalid arguments")
	}
	// How many slaves are there?
	slaveCount, _ := mpi.CommSize()
	slaveCount--
	// Make some matrices to multiply. Go isn't smart enough to
	// know that the result of this is a zero matrix
	i, err := strconv.Atoi(args[1])
	if err != nil {
		panic(err.Error())
	}
	j, err := strconv.Atoi(args[2])
	if err != nil {
		panic(err.Error())
	}
	k, err := strconv.Atoi(args[3])
	if err != nil {
		panic(err.Error())
	}
	mat1 := matrix.NewMatrix(i, j)
	mat2 := matrix.NewMatrix(j, k)
	// Test latency/bandwidth
	message = make([]int, mat1.Cols)
	startTime := time.Now()
	//fmt.Print("Ping\n")
	err = mpi.Send(&message, slaveCount, 0)
	if err != nil {
		panic(err.Error())
	}
	//fmt.Print("Pong\n")
	_, err = mpi.Recv(&message, slaveCount, 0)
	if err != nil {
		panic(err.Error())
	}
	roundTrip := time.Since(startTime)
	//fmt.Printf("Ping: %v\n", roundTrip)
	// Use latency to calculate optimal matrix size
	size := calcMatrixSize(mat1.Cols, roundTrip)
	//fmt.Printf("Optimal size: %d\n", size)
	// Start the timer
	startTime = time.Now()

	// Prep variables
	result := matrix.NewMatrix(mat1.Rows, mat2.Cols)
	generate, merge := makeGenMerge(mat1, mat2, result, size)

	// Run loop for responding to messages
	var slave int
	for state := MStart; state != MDead; {
		switch state {
		case MStart:
			// Receiving message...
			//fmt.Println("Master: Receiving message...")
			_, err := mpi.Recv(&message, mpi.AnySource, mpi.AnyTag)
			if err != nil {
				panic(err.Error())
			}
			state = MReceivedReady
		case MReceivedReady:
			// Parsing message...
			//fmt.Printf("Master: Received %T\n", message)
			switch m := message.(type) {
			case WorkRequest:
				work := generate(m)
				message = work
				slave = m.Slave
				mpi.Send(&message, slave+1, 0)
				state = MStart
			case Result:
				if merge(m) {
					state = MGotKillSignal
				} else {
					state = MStart
				}
			case MatrixRequest:
				matPair := make([]matrix.Matrix, 2)
				matPair[0] = *mat1
				matPair[1] = *mat2
				message = matPair
				mpi.Send(&message, m.Slave+1, 0)
				state = MStart
			default:
				errMessage :=
					fmt.Sprintf(
						"Invalid message given that there is still work (%T).",
						m)
				panic(errors.New(errMessage))
			}
		case MGotKillSignal:
			// Kill signal received...
			mpi.Recv(&message, mpi.AnySource, mpi.AnyTag)
			switch m := message.(type) {
			case WorkRequest:
				slave = m.Slave
				state = MReceivedDyingWorkRequest
			case MatrixRequest:
				matPair := make([]matrix.Matrix, 2)
				matPair[0] = *mat1
				matPair[1] = *mat2
				message = matPair
				mpi.Send(&message, m.Slave+1, 0)
			case Result:
				state = MGotKillSignal
			case KillACK:
				//fmt.Printf("Master: %d slaves left\n", slaveCount)
				slaveCount--
				if slaveCount <= 0 {
					state = MDead
				}
			default:
				errMessage :=
					fmt.Sprintf(
						"Invalid message given that we're shutting down (%T).",
						m)
				panic(errors.New(errMessage))
			}
		case MReceivedDyingWorkRequest:
			// Received work request while dying...
			message = Kill{}
			mpi.Send(&message, slave+1, 0)
			state = MGotKillSignal
		}
	}

	// Print the time taken
	fmt.Print(time.Since(startTime).Seconds())
}

// I know that assuming that all computers are equally fast is not
// perfect, but this gives an order of magnitude approximation.
func calcMatrixSize(innerDim int, roundTrip time.Duration) (size int) {
	matTime := time.Duration(0)
	for size = 10; matTime.Seconds() > roundTrip.Seconds(); size <<= 1 {
		m1 := matrix.NewMatrix(size, innerDim)
		m2 := matrix.NewMatrix(innerDim, size)
		startTime := time.Now()
		m1.Multiply(m2)
		matTime = time.Since(startTime)
	}
	return size
}

type generator func(WorkRequest) Work
type merger func(Result) bool

func makeGenMerge(mat1, mat2, result *matrix.Matrix, size int) (generator, merger) {
	//queue := make(chan Work)
	//requests := make(chan WorkRequest)
	//go masterGenerator(mat1, mat2, size, queue, requests)
	i := 0
	j := 0
	nextRow := 0
	nextCol := 0
	var unmerged ResultPQ
	result = matrix.NewMatrix(mat1.Rows, mat2.Cols)

	generate := func(wr WorkRequest) (w Work) {
		w.Slave = wr.Slave
		for count := 0; count < wr.Amount; count++ {
			w.Requests =
				append(w.Requests, WorkUnit{i, j, size})
			j += size
			if j >= mat2.Cols {
				j = 0
				i += size
			}
			if i >= mat1.Rows {
				i = nextRow
				j = nextCol
			}
		}
		return w
	}

	merge := func(r Result) (done bool) {
		tmpResult := new(Result)
		*tmpResult = r
		heap.Push(&unmerged, tmpResult)
		// Delete results that have already been seen
		for unmerged.Len() > 0 &&
			(unmerged[0].Row < nextRow ||
				unmerged[0].Row == nextRow &&
					unmerged[0].Col < nextCol) {
			heap.Pop(&unmerged)
		}
		//fmt.Printf("Master: ready to merge %v at %d, %d (%d, %d)\n", r, r.Row, r.Col, nextRow, nextCol)
		for unmerged.Len() > 0 &&
			unmerged[0].Row == nextRow &&
			unmerged[0].Col == nextCol {
			// Move the next result into the final matrix
			next := heap.Pop(&unmerged).(*Result)
			for i := 0; i < next.Matrix.Rows; i++ {
				for j := 0; j < next.Matrix.Rows; j++ {
					result.Set(next.Row+i, next.Col+j, next.Matrix.Get(i, j))
				}
			}
			// Increment the next row and column
			nextCol += next.Matrix.Cols
			if nextCol >= result.Cols {
				nextCol = 0
				nextRow += next.Matrix.Rows
			}
		}
		if nextRow >= result.Rows {
			return true
		}
		return false
	}

	return generate, merge
}

func slave1() {
	// Test latency
	var incoming interface{}
	//fmt.Print("Slave: Ping\n")
	_, err := mpi.Recv(&incoming, 0, 0)
	if err != nil {
		panic(err.Error())
	}
	//fmt.Print("Slave: Pong\n")
	err = mpi.Send(&incoming, 0, 0)
	if err != nil {
		panic(err.Error())
	}
	slave()
}

func slave() {
	rank, _ := mpi.CommRank()
	//fmt.Printf("Slave %d starting\n", rank-1)
	// Spawn goroutines
	finished := make(chan Result, realMaxQueue)
	workQueue := make(chan interface{}, realMaxQueue)
	go slaveWorker(finished, workQueue)

	// Loop forever
	busy := 0
	maxQueue := 1
	var result Result
	var work Work
	var message interface{}
	for state := SRequestingMatrix; state != SDead; {
		switch state {
		case SRequestingMatrix:
			message = MatrixRequest{rank - 1}
			//fmt.Println("Slave: Requesting matrix...")
			mpi.Send(&message, 0, 0)
			state = SRequestedMatrix
		case SRequestedMatrix:
			//fmt.Println("Slave: Requested matrix...")
			mpi.Recv(&message, 0, 0)
			switch m := message.(type) {
			case []matrix.Matrix:
				mat1 = m[0]
				mat2 = m[1]
				state = SStart
				//fmt.Println("Slave: Received matrix")
			default:
				panic(fmt.Sprintf("Expected pair of matrices, got %T", m))
			}
		case SStart:
			// Find out if we have any finished work
			select {
			case result = <-finished:
				state = SGotResult
			default:
				state = SNoFinished
			}
		case SGotResult:
			// Send back the finished work
			message = result
			mpi.Send(&message, 0, 0)
			busy--
			state = SStart
			// If we are completely out of work, we should probably ask
			// for more next time
			if busy == 0 && maxQueue < realMaxQueue {
				maxQueue *= 2 // Based on TCP slow start
				// Drop it down if we got too high
				if maxQueue > realMaxQueue {
					maxQueue = realMaxQueue
				}
				state = SNoFinished
			}
		case SNoFinished:
			// Ask for work if we need it; wait if we don't.
			if busy <= maxQueue/2 {
				message = WorkRequest{Slave: rank - 1, Amount: maxQueue - busy}
				mpi.Send(&message, 0, 0)
				state = SRequestedWork
			} else {
				time.Sleep(time.Second / 10)
				state = SStart
			}
		case SRequestedWork:
			// Receive stuff from the master and act appropriately
			mpi.Recv(&message, 0, 0)
			//fmt.Printf("Slave %d: Received %T\n", rank-1, message)
			switch m := message.(type) {
			case Work:
				work = m
				state = SReceivedWork
			case NoWork:
				state = SReceivedNothing
			case Kill:
				state = SReceivedKill
			default:
				panic(fmt.Sprintf("Slave: Invalid response to request (%T)", m))
			}
		case SReceivedWork:
			// Send to work to a worker
			workQueue <- work
			busy += len(work.Requests)
			state = SStart
		case SReceivedNothing:
			// If we've received nothing, then the master is out of work
			// Let's wait a bit for the master to tell us to die, maybe we
			// still have unfinished work
			time.Sleep(time.Second / 10)
			state = SStart
		case SReceivedKill:
			// Send the kill order to the worker
			workQueue <- Kill{}
			state = SWorkerNotYetDead
		case SWorkerNotYetDead:
			// Wait for the worker to die, letting it empty its queue
			if _, ok := <-finished; ok {
				state = SWorkerNotYetDead
			} else {
				state = SKilledWorker
			}
		case SKilledWorker:
			// Tell the master that we're dead
			message = KillACK{}
			mpi.Send(&message, 0, 0)
			state = SDead
		}
	}
	//fmt.Printf("Slave %d: Done.\n", rank-1)
}

// Multiply matrices
func slaveWorker(main chan Result, workQueue chan interface{}) {
	// Loop forever
	for notDone := true; notDone; {
		switch message := (<-workQueue).(type) {
		// Work:
		case Work:
			// Do the work and send it back
			for _, unit := range message.Requests {
				main <- unit.Process()
			}
		// Kill signal:
		case Kill:
			// Die
			notDone = false
		}
	}
	close(main)
}

// Message types
type Kill struct{}

type KillACK struct{}

type Result struct {
	Row, Col int
	Matrix   matrix.Matrix
}

type WorkUnit struct {
	Row, Col, Size int
}

var mat1 matrix.Matrix
var mat2 matrix.Matrix
var leftCache = make(map[int]matrix.Matrix)
var rightCache = make(map[int]matrix.Matrix)

func (wu WorkUnit) Process() (result Result) {
	// Get factors
	var leftFactor, rightFactor matrix.Matrix
	var hit bool
	if leftFactor, hit = leftCache[wu.Col]; !hit {
		leftFactor =
			mat1.GetSubMatrix(
				wu.Row, 0,
				wu.Row+wu.Size-1, mat1.Cols-1)
		leftCache[result.Row] = leftFactor
	}
	if rightFactor, hit = rightCache[wu.Col]; !hit {
		rightFactor =
			mat2.GetSubMatrix(
				0, wu.Col,
				mat2.Rows-1, wu.Col+wu.Size-1)
		rightCache[result.Col] = rightFactor
	}
	// Processing submatrix
	result.Row = wu.Row
	result.Col = wu.Col
	result.Matrix = *leftFactor.Multiply(&rightFactor)
	return result
}

type Work struct {
	Slave    int
	Requests []WorkUnit
}

type WorkRequest struct {
	Slave, Amount int
}

type MatrixRequest struct {
	Slave int
}

type NoWork struct{}

// Priority queue for completed work
type ResultPQ []*Result

func (pq ResultPQ) Len() int { return len(pq) }

func (pq ResultPQ) Less(i, j int) bool {
	return pq[i].Row < pq[j].Row || (pq[i].Row == pq[j].Row && pq[i].Col < pq[j].Col)
}

func (pq ResultPQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *ResultPQ) Push(x interface{}) {
	tmp := *pq
	tmp = append(*pq, x.(*Result))
	*pq = tmp
}

func (pq *ResultPQ) Pop() interface{} {
	x, tmpPQ := (*pq)[len(*pq)-1], (*pq)[:len(*pq)-1]
	*pq = tmpPQ
	return x
}
